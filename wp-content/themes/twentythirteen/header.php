<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">	
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="icon" type="image/png" href="wp-content/themes/twentythirteen/images/fav_house.png" />

<meta name="viewport" content="width=1150, user-scalable=yes">
	
<meta name="HandheldFriendly" content="true">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
	<?php wp_head(); ?>
	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<link href="wp-content/themes/twentythirteen/Hover-master/css/hover.css" rel="stylesheet" media="all">
	<script type="text/javascript">
		$("#myImage").mouseover(function () {
			$(this).attr("class", "image-replacement");
		});
		$("#myImage").mouseout(function () {
			$(this).attr("class", "ClassBeforeImage-replacement");
		});
	</script>
	<style type="text/css">
		@font-face{
			font-family: GothaProReg;
			src: url("wp-content/themes/twentythirteen/fonts/GothaProReg.otf") format("opentype");
		}
		@font-face {
			font-family: GothaProNarMed;
			src: url("wp-content/themes/twentythirteen/fonts/GothaProNarMed.otf") format("opentype");
		}
		@font-face {
			font-family: GothaProNarBol;
			src: url("wp-content/themes/twentythirteen/fonts/GothaProNarBol.otf") format("opentype");
		}
		@font-face {
			font-family: GothaProMedIta;
			src: url("wp-content/themes/twentythirteen/fonts/GothaProMedIta.otf") format("opentype");
		}
		@font-face {
			font-family: GothaProMed;
			src: url("wp-content/themes/twentythirteen/fonts/GothaProMed.otf") format("opentype");
		}
		@font-face {
			font-family: GothaProLigIta;
			src: url("wp-content/themes/twentythirteen/fonts/GothaProLigIta.otf") format("opentype");
		}
		@font-face {
			font-family: GothaProLig;
			src: url("wp-content/themes/twentythirteen/fonts/GothaProLig.otf") format("opentype");
		}
		@font-face {
			font-family: GothaProIta;
			src: url("wp-content/themes/twentythirteen/fonts/GothaProIta.otf") format("opentype");
		}
		@font-face {
			font-family: GothaProBolIta;
			src: url("wp-content/themes/twentythirteen/fonts/GothaProBolIta.otf") format("opentype");
		}
		@font-face {
			font-family: GothaProBol;
			src: url("wp-content/themes/twentythirteen/fonts/GothaProBol.otf") format("opentype");
		}
		@font-face {
			font-family: GothaProBlaIta;
			src: url("wp-content/themes/twentythirteen/fonts/GothaProBlaIta.otf") format("opentype");
		}
		@font-face {
			font-family: GothaProBla;
			src: url("wp-content/themes/twentythirteen/fonts/GothaProBla.otf") format("opentype");
		}
		html,
		button,
		input,
		select,
		textarea {
			font-family: GothaProReg, Tahoma, sans-serif;
		}
		h1,
		h2,
		h3,
		h4,
		h5,
		h6 {
			clear: both;
			font-family: GothaProBol, Georgia, serif;
			line-height: 1.3;
		}

		.mainContent
		{
			width: 1140px;
			height: 1000px;
			margin: 0 auto 0;
		}
		.c_1
		{
			background: url("wp-content/themes/twentythirteen/images/bg_1.png");
			max-width: 1920px;
			width: 100%;
			min-width: 1140px;
			height: 1000px;
		}
		.c_2
		{
			background: url("wp-content/themes/twentythirteen/images/bg_2.png");
			max-width: 1920px;
			width: 100%;
			min-width: 1140px;
			height: 1000px;
			perspective: 1px;
			overflow-x: hidden;
			overflow-y: auto;
		}
		.parallax {
			perspective: 1px;
			height: 100vh;
			overflow-x: hidden;
			overflow-y: auto;
		}
		.parallax__layer {
			position: absolute;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
		}
		.parallax__layer--base {
			transform: translateZ(0);
		}
		.parallax__layer--back {
			transform: translateZ(-1px);
		}
		.c_3
		{
			background: url("wp-content/themes/twentythirteen/images/bg_3.png");
			max-width: 1920px;
			width: 100%;
			min-width: 1140px;
			height: 1020px;
		}
		.c_4
		{
			background: url("wp-content/themes/twentythirteen/images/bg_4.png");
			max-width: 1920px;
			width: 100%;
			height: 1150px;
			min-width: 1140px;
		}
		.c_5
		{
			background: url("wp-content/themes/twentythirteen/images/bg_price.png");
			max-width: 1920px;
			width: 100%;
			height: 840px;
			min-width: 1140px;
		}
		.inlineRow
		{
			display: inline-block;

		}
		/*			FIRST */
		
		.c_1_1
		{
			color: #252525;
		}
		.c_1_1_2
		{
			font-family: GothaProMed;
			font-size: 20px;
			margin-top: 60px;
			float: right;
			text-align: right;
		}
		.c_1_1_2 li {
			margin-bottom: 25px;
		}
		.c_1 .logo
		{
			text-align: left;
			margin-top: 35px;
		}
		.c_1_1_2 img {
			margin-left: 29px;
		}
		ul li
		{
			 list-style-type: none;
		}
		.clrOrange
		{
			color:#cf5c25;
		}
		.c_1_2
		{
			font-family: GothaProBol;
			font-size: 69.5px;
			color:#cf5c25;
			text-align: center;
			margin: 0;
			padding: 0;
		}
		.c_1_3
		{
			font-family: GothaProLig;
			font-size: 65px;
			text-align: center;
			margin: 25px 0 110px 0;
			color: #898989;
			padding: 0;
		}
		.c_1_4
		{
			font-family: GothaProReg;
			font-size: 40px;	
			text-align: center;
			margin: 0;
			color: #252525;
			padding: 0;
		}
		.c_1_5 {
			margin: 50px 0 0 0;
		}
		.c_1_5 .c_1_5_img
		{
			margin: 0 40px 0 0;
		}
		.c_1_5 .c_1_5_img_last
		{
			margin: 0!important;
		}
		.c_1_bg_img
		{
			margin: -350px 0 0 0;
		}
		img.c_1_bg_img_1 {
			margin: 0 0 0 -380px;	
		}
		img.c_1_bg_img_2 {
		margin: 0 -929px 0 1075px;
		}

		/*			SECOND */
		.c_2_1
		{
			text-transform: uppercase;
			font-family: GothaProMed;
			font-size: 50px;	
			text-align: center;
			margin: 0;
			color: white;
			padding: 120px 0 0 0;
		}
		.c_2_2
		{
			text-align: center;
		}
		.c_2_3
		{
			font-family: GothaProReg;
			font-size: 20px;
			text-align: left;
			margin: 45px auto 45px;
			width: 1050px;
			color: white;
			padding: 0;
		}
		article#post-23 {
			margin: 0 0 0 -450px;
		}
		div#panel-23-0-0-0 {
		background: none;
		}
		.textwidget h3 {
			font-family: GothaProMed;
			font-weight: 400;
			color: white;
			font-size: 40px;
		}
		/*				THIRD */
		.c_3_bg_img 
		{
			z-index: 1;
			margin: 130px 0 -1100px -310px;
		}
		.c_3_1
		{
			text-transform: uppercase;
			font-family: GothaProMed;
			font-size: 50px;
			z-index: 4;
			text-align: center;
			margin: 0;
			color: white;
			padding: 120px 0 0 0;
		}
		.c_3_2
		{
			text-align: center;
		}
		.c_3_3 img
		{
			border-radius: 50%;
			z-index: 999;
			border: 4px solid white;
		}
		.c_3_3 {
			margin: -68px 0 0 0;
		}
		/*				FOURTH	*/
		.c_4_1
		{
			text-transform: uppercase;
			font-family: GothaProMed;
			font-size: 30px;
			z-index: 4;
			text-align: center;
			margin: 0;
			color: white;
			padding: 80px 0 5px 0;
		}
		.c_4_2
		{
			text-transform: uppercase;
			font-family: GothaProLig;
			font-size: 30px;
			z-index: 4;
			text-align: center;
			margin: 0;
			color: white;
			padding: 0 0 0;
		}
		input{
			font-family: GothaProReg, Tahoma, sans-serif;
			border: none;
			text-align: center;
			height: 50px;
			width: 615px;
			margin: 0 40px;
			background: url("wp-content/themes/twentythirteen/images/bg_second.png");
		}
		.c_3_form
		{
			width: 700px;
			border: 2px solid white;
			text-align: center;
			font-family: GothaProMed;
			margin: -85px 0 0 160px;
		}
		input.wpcf7-form-control.wpcf7-submit {
			width: 380px;
			font-size: 25px;
			font-family: GothaProMed;
			height: 70px;
			text-transform: uppercase;
		}
		:-ms-input-placeholder, ::-moz-placeholder, :-moz-placeholder, ::-webkit-input-placeholder {
		   color: red;
		   color: #252525;
		}
		#c_1_bg_img_ps
		{
			background: url("wp-content/themes/twentythirteen/images/bg_1_img_2.png");
			width: 410px;
			height: 535px;
			display: inline;
			background-size: 100%;
			position: fixed;
			margin: 0 -1105px 0 1105px;
			margin-top: -350px;
		}
		body
		{
			overflow-y:hidden;
			overflow-x:hidden;
		}
		ymaps.ymaps-map.ymaps-i-ua_js_yes {
			width: 690px!important;
			border-radius: 50%;
			height: 690px!important;
		}
		ymaps.ymaps-layers-pane {
			left: 315px!important;
			top: 340px!important;
		}
		.c_4_3 {
			margin: 50px auto 0;
			text-align: center;
			
		}
		ymaps {
			padding: 0!important;
			margin: 0 auto 0!important;
		}
		.c_4_4
		{
			text-transform: uppercase;
			font-family: GothaProMed;
			font-size: 30px;
			width: 180px;
			color: white;
			-ms-transform: rotate(325deg);
			-webkit-transform: rotate(325deg);
			transform: rotate(325deg);
			margin: 110px 0 0 0px;
			padding: 0;
			position: absolute;
		}
		.c_4_4 img {
			margin: 30px 0px 0 50px;
		}
		.c_5_1
		{
			font-size: 50px;
			text-transform: uppercase;
			font-family: GothaProMed;
			text-align: center;
			margin: 0 auto 0;
			color: white;
			padding: 50px 0 40px 0;
		}
		.c_5_2
		{
			width: 215px;
			height: 6px;
			text-align: center;
			margin: 0 auto 0;
			background: white;
		}
		.c_5_3
		{
			background: url("wp-content/themes/twentythirteen/images/price_clock.png");
			width: 1139px;
			color: white;
			height: 328px;
			margin: 0 auto 0;
			text-align: center;
		}
		.c_5_4
		{
			font-size: 20px;
			color: white;
			font-family: GothaProReg;
			margin: 25px auto 0;
		}
		.c_5_3_1, .c_5_3_2
		{
			text-align: center;
			display: inline-block;
			color: white;
			padding: 100px 0 0 0;
		}
		.c_5_3_1_1, .c_5_3_2_1
		{
			font-size: 90px;
			margin: 0;
			padding: 0;
			line-height: 65px;
		}
		.c_5_3_1_2,.c_5_3_2_2
		{
			font-size: 25px;
		}
		.c_5_3_1_3,.c_5_3_2_3
		{
			font-size: 25px;
			padding: 23px 0 0 0;
			line-height: 25px;
		}
		.c_5_3_1 {
			margin: 0 0 0 115px;
		}
		.c_5_3_2 {
			margin: 0 -225px 0 225px;
		}
		@media only screen and (max-device-width: 480px) 
		{
			.c_1_bg_img
			{
				display: none;
			}
			body
			{
				overflow-y:visible!important;
				overflow-x:visible!important;
			}
			.lightbox, .lb-nav, .lb-dataContainer {
			  	width: 1140px!important;
			  }

		}
		#map {
			width: 100%;
			margin: 0 auto 0;
			height: 100%;
		}
		.c_2_3, input, input.wpcf7-form-control.wpcf7-submit, #c_1_bg_img_ps,.c_5_4, ymaps.ymaps-map.ymaps-i-ua_js_yes,.c_4_4,.c_5_3
		{
			width: 80%;
		}

	</style>



		<link href="wp-content/themes/twentythirteen/lightbox/css/lightbox.css" rel="stylesheet" />
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc&sensor=false"></script>
	<script type="text/javascript">
		function calculateRoute(from, to) {
			// Center initialized to Naples, Italy
			var myOptions = {
				zoom: 0,
				center: new google.maps.LatLng(40.84, 14.25),
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			var mapOptions = {
				// How zoomed in you want the map to start at (always required)
				zoom: 0,

				// The latitude and longitude to center the map (always required)
				center: new google.maps.LatLng(55.836697, 37.63753889999998), // New York

				// How you would like to style the map.
				// This is where you would paste any style found on Snazzy Maps.
				styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"administrative.neighborhood","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"simplified"},{"color":"#797979"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#151515"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#151515"}]},{"featureType":"landscape.natural.landcover","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry","stylers":[{"color":"#151515"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.fill","stylers":[{"color":"#151515"}]},{"featureType":"landscape.natural.terrain","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"color":"#151515"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry.fill","stylers":[{"color":"#151515"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"lightness":21},{"color":"#151515"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#151515"}]},{"featureType":"poi","elementType":"geometry.stroke","stylers":[{"color":"#151515"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.highway.controlled_access","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#be3e08"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#be3e08"},{"lightness":18},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#000000"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"off"},{"color":"#ff0000"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#be3e08"},{"lightness":16}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#000000"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17},{"visibility":"on"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#151515"}]}]
			};
			// Draw the map
			var mapObject = new google.maps.Map(document.getElementById("map"), mapOptions);

			var directionsService = new google.maps.DirectionsService();
			var directionsRequest = {
				origin: from,
				destination: to,
				travelMode: google.maps.DirectionsTravelMode.DRIVING,
				unitSystem: google.maps.UnitSystem.METRIC
			};
			directionsService.route(
				directionsRequest,
				function(response, status)
				{
					if (status == google.maps.DirectionsStatus.OK)
					{
						new google.maps.DirectionsRenderer({
							map: mapObject,
							directions: response
						});
					}
					else
						$("#error").append("Unable to retrieve your route<br />");
				}
			);
		}

		$(document).ready(function() {
			// If the browser supports the Geolocation API
			if (typeof navigator.geolocation == "undefined") {
				$("#error").text("Your browser doesn't support the Geolocation API");
				return;
			}

			$("#from-link, #to-link").click(function(event) {
				event.preventDefault();
				var addressId = this.id.substring(0, this.id.indexOf("-"));

				navigator.geolocation.getCurrentPosition(function(position) {
						var geocoder = new google.maps.Geocoder();
						geocoder.geocode({
								"location": new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
							},
							function(results, status) {
								if (status == google.maps.GeocoderStatus.OK)
									$("#" + addressId).val(results[0].formatted_address);
								else
									$("#error").append("Unable to retrieve your address<br />");
							});
					},
					function(positionError){
						$("#error").append("Error: " + positionError.message + "<br />");
					},
					{
						enableHighAccuracy: true,
						timeout: 10 * 1000 // 10 seconds
					});
			});

			calculateRoute("Selskokhozyaystvennaya ulitsa, 15 корпус 2, Moscow,129226 ‎","Selskokhozyaystvennaya ulitsa, 15 корпус 2, Moscow,129226 ‎");

		});
	</script>




	<style>

		/* Parallax base styles
          --------------------------------------------- */

		.parallax {
			height: 100vh;
			overflow-x: hidden;
			overflow-y: auto;
			-webkit-perspective: 300px;
			perspective: 300px;
		}

		.parallax__group {
			position: relative;
			height: 500px; /* fallback for older browsers */
			height: 100vh;
			-webkit-transform-style: preserve-3d;
			transform-style: preserve-3d;

		}

		.parallax__layer {
			position: absolute;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
		}

		.parallax__layer--fore {
			-webkit-transform: translateZ(90px) scale(.7);
			transform: translateZ(90px) scale(.7);
			z-index: 1;
		}

		.parallax__layer--base {
			-webkit-transform: translateZ(0);
			transform: translateZ(0);
			z-index: 4;
		}

		.parallax__layer--back {
			-webkit-transform: translateZ(-300px) scale(2);
			transform: translateZ(-300px) scale(2);
			z-index: 3;
		}

		.parallax__layer--deep {
			-webkit-transform: translateZ(-600px) scale(3);
			transform: translateZ(-600px) scale(3);
			z-index: 2;
		}


		/* Debugger styles - used to show the effect
          --------------------------------------------- */

		.debug {
			position: fixed;
			top: 0;
			left: .5em;
			z-index: 999;
			background: rgba(0,0,0,.85);
			color: #fff;
			padding: .5em;
			border-radius: 0 0 5px 5px;
		}
		.debug-on .parallax__group {
			-webkit-transform: translate3d(800px, 0, -800px) rotateY(30deg);
			transform: translate3d(700px, 0, -800px) rotateY(30deg);
		}
		.debug-on .parallax__layer {
			box-shadow: 0 0 0 2px #000;
			opacity: 0.9;
		}
		.parallax__group {
			-webkit-transition: -webkit-transform 0.5s;
			transition: transform 0.5s;
		}

		.parallax {
			font-size: 200%;
		}

		/* centre the content in the parallax layers */
		.title {
			text-align: center;
			position: absolute;
			left: 50%;
			top: 50%;
			-webkit-transform: translate(-50%, -50%);
			transform: translate(-50%, -50%);
		}



		/* style the groups
          --------------------------------------------- */

		#group1 {
			z-index: 5; /* slide over group 2 */
			height: 974px;
		}
		#group1 .parallax__layer--base {

			background: url("wp-content/themes/twentythirteen/images/bg_1.png");

		}

		#group2 {
			z-index: 3; /* slide under groups 1 and 3 */
			height: 1000px;
		}
		#group2 .parallax__layer--back {
			background: url("wp-content/themes/twentythirteen/images/bg_2.png");

		}

		#group3 {
			z-index: 4; /* slide over group 2 and 4 */
			height: 1070px;
		}
		#group3 .parallax__layer--base {
			background: url("wp-content/themes/twentythirteen/images/bg_3.png");

		}

		#group4 {
			z-index: 2; /* slide under group 3 and 5 */
			height: 840px;
		}
		#group4 .parallax__layer--back {
			background: url("wp-content/themes/twentythirteen/images/bg_price.png");
			height: 1000px;
		}

		#group5 {
			z-index: 3; /* slide over group 4 and 6 */
			height: 1300px;
			position: relative;
		}
		#group5 .parallax__layer--base {
			background: url("wp-content/themes/twentythirteen/images/bg_4.png");

		}

		#group6 {
			z-index: 2; /* slide under group 5 and 7 */
		}
		#group6 .parallax__layer--back {
			background: rgb(245,235,100);
		}

		#group7 {
			z-index: 3; /* slide over group 7 */
		}
		#group7 .parallax__layer--base {
			background: rgb(255,241,100);
		}


		/* misc
          --------------------------------------------- */
		.demo__info {
			position: absolute;
			z-index:100;
			bottom: 1vh;
			top: auto;
			font-size:80%;
			text-align:center;
			width: 100%;
		}

		@media only screen and (max-device-width: 480px)
		{
			html,body
			{
				height:100vh;
				overflow:hidden;
			}
			.parallax
			{
				-webkit-overflow-scrolling: touch; overflow-y:auto;
			}
		}

		.c_5_3 {
		width: 100%;
		}
	</style>
</head>

<body style="min-width:1100px" <?php //body_class(); ?>>
	<!--
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
			<a class="home-link" href="<?php //echo esc_url( home_url( '/' ) ); ?>" title="<?php //echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<h1 class="site-title"><?php //bloginfo( 'name' ); ?></h1>
				<h2 class="site-description"><?php //bloginfo( 'description' ); ?></h2>
			</a>

			<div id="navbar" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<button class="menu-toggle"><?php // _e( 'Menu', 'twentythirteen' ); ?></button>
					<a class="screen-reader-text skip-link" href="#content" title="<?php //esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php //_e( 'Skip to content', 'twentythirteen' ); ?></a>
					<?php //wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
					<?php //get_search_form(); ?>
				</nav>
			</div>
		</header>

		<div id="main" class="site-main">
-->