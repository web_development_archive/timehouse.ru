<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 * Template Name: Landing 
 * wp-content/themes/twentythirteen/images/
 */
get_header(); ?>

<div class="parallax">
	<div id="group1" class="parallax__group">
		<div class="parallax__layer parallax__layer--base">
			<div class="mainContent">
				<div class="c_1_1">
					<img class="logo" src="wp-content/themes/twentythirteen/images/logo.png"  alt/>
					<ul class="c_1_1_2">
						<li> <div class="inlineRow">8</div> <div class="clrOrange inlineRow">(499)</div> <div class="inlineRow"> 189-87-92  </div><img src="wp-content/themes/twentythirteen/images/icons_2.png" alt/>
						</li>
						<li>anticafe-timehouse@mail.ru <img src="wp-content/themes/twentythirteen/images/icons_1.png" alt />
						</li>
					</ul>
				</div>
				<div class="c_1_2">Time house</div>
				<div class="c_1_3">место где интересно</div>
				<div class="c_1_4">Чем мы будем заниматься?</div>
				<div class="c_1_5">
					<a class="c_1_5_a"><img class="hvr-buzz-out c_1_5_img" src="wp-content/themes/twentythirteen/images/whatwilldo1.png" alt></a>
					<a class="c_1_5_a"><img class="hvr-buzz-out c_1_5_img" src="wp-content/themes/twentythirteen/images/whatwilldo2.png" alt></a>
					<a class="c_1_5_a"><img class="hvr-buzz-out c_1_5_img" src="wp-content/themes/twentythirteen/images/whatwilldo3.png" alt></a>
					<a class="c_1_5_a"><img class="hvr-buzz-out c_1_5_img" src="wp-content/themes/twentythirteen/images/whatwilldo4.png" alt></a>
					<a class="c_1_5_a"><img class="hvr-buzz-out c_1_5_img c_1_5_img_last" src="wp-content/themes/twentythirteen/images/whatwilldo5.png" alt></a>
				</div>
				<!--<div id="c_1_bg_img_ps"></div>-->
				<div class="c_1_bg_img">
					<img class="c_1_bg_img_1" src="wp-content/themes/twentythirteen/images/bg_1_img_1.png" alt />
					<img class="c_1_bg_img_2" src="wp-content/themes/twentythirteen/images/bg_1_img_2.png" alt />
				</div>
			</div>
		</div>
	</div>
	<div id="group2" class="parallax__group">
		<div class="parallax__layer parallax__layer--base">
			<div class="mainContent">
				<div class="c_2_1">
					Немного о нашей тусовке
				</div>
				<div class="c_2_2"><img src="wp-content/themes/twentythirteen/images/linesecond.png" alt></div>
				<div class="c_2_3">
					В самый разгар зимы в Москве начинает свою работу заведение формата АнтиКафе – TimeHouse Cafe. В TimeHouse Cafe каждый сможет найти что-то для себя: новое и приятное или давно ожидаемое. <br />
					Для всех и для каждого: хороший кофе, вкусный чай, конфеты и печенье, всевозможные снеки,
					персональный кинозал для просмотра любимых фильмов, Wi-Fi, 2 PS3 и множество игр,
					куча книг и большой выбор настольных игр.
					Здесь каждый сможет найти для себя развлечение по душе или же устроившись на удобном диване поработать в спокойной обстановке. <br />
					Для студентов творческих ВУЗов у нас есть интересные предложения для сотрудничества. <br />

					Каждую пятницу после 17:00 Cork Fee: возможность принести и распивать вино. Пробочный сбор 100 р.
					<br />
					Придя в TimeHouse Cafe, вы платите не за еду и напитки, не за развлечения, а за время и пространство, которое позволяет работать, отдыхать и проводить время с удовольствием.
					<br />
					Ждем вас каждый день с 9:00 до 20:00.
				</div>
				<div class="c_2_4">
					<?php while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<!--<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<h1 class="entry-title"><?php the_title(); ?></h1>
					</header>.entry-header -->

							<div class="entry-content">
								<?php the_content(); ?>
								<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
							</div><!-- .entry-content -->

							<footer class="entry-meta">
								<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
							</footer><!-- .entry-meta -->
						</article><!-- #post -->

						<?php comments_template(); ?>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
		<div class="parallax__layer parallax__layer--back">
		</div>
	</div>
	<div id="group3" class="parallax__group">
		<div class="parallax__layer parallax__layer--base">
			<div class="mainContent">
				<img class="c_3_bg_img" src="wp-content/themes/twentythirteen/images/img_3_bg.png" alt />
				<div class="c_3_1">
					фотоотчет
				</div>
				<div class="c_3_2"><img src="wp-content/themes/twentythirteen/images/linesecond.png" alt /></div>
				<div class="c_3_3">
					<a class="example-image-link" href="wp-content/themes/twentythirteen/images/1.jpg" data-lightbox="example-set" data-title="Для выхода нажмите на любую облать или на Х"><img style="width:445px; height:445px;" class="example-image" src="wp-content/themes/twentythirteen/images/1.jpg" alt=""/></a>

					<a class="example-image-link" href="wp-content/themes/twentythirteen/images/2.jpg" data-lightbox="example-set" data-title="Для выхода нажмите на любую облать или на Х"><img style="margin:45px 0 0 -125px; width:400px; height:400px;" class="example-image" src="wp-content/themes/twentythirteen/images/2.jpg" alt="" /></a>

					<a class="example-image-link" href="wp-content/themes/twentythirteen/images/3.jpg" data-lightbox="example-set" data-title="Для выхода нажмите на любую облать или на Х"><img style="margin: 0px -133px 0 -94px; width:530px; height:530px;" class="example-image" src="wp-content/themes/twentythirteen/images/3.jpg" alt="" /></a>

					<a class="example-image-link" href="wp-content/themes/twentythirteen/images/4.jpg" data-lightbox="example-set" data-title="Для выхода нажмите на любую облать или на Х"><img style="margin: -300px 0 0 51px; width:475px; height:475px;" class="example-image" src="wp-content/themes/twentythirteen/images/4.jpg" alt="" /></a>

					<a class="example-image-link" href="wp-content/themes/twentythirteen/images/5.jpg" data-lightbox="example-set" data-title="Для выхода нажмите на любую облать или на Х"><img style="margin: -91px 48px 0 -16px; width:400px; height:400px;" class="example-image" src="wp-content/themes/twentythirteen/images/5.jpg" alt="" /></a>
				</div>
			</div>
		</div>
	</div>
	<div id="group4" class="parallax__group">
		<div class="parallax__layer parallax__layer--base">
			<div class="mainContent" style="    margin: 50px auto 0;">
				<div class="c_5_1">
					Наши смешные цены
				</div>
				<div class="c_5_2"></div>
				<div class="c_5_3">
					<div class="c_5_3_1">
						<div class="c_5_3_1_1">2</div>
						<div class="c_5_3_1_2">рубля /минута</div>
						<div class="c_5_3_1_3">первые<br />два часа</div>
					</div>
					<div class="c_5_3_2">
						<div class="c_5_3_2_1">1.5</div>
						<div class="c_5_3_2_2">рубля /минута</div>
						<div class="c_5_3_2_3">последующие<br />часы</div>
					</div>
				</div>
				<div class="c_5_4">
					Антикафе Time House представляет собой сочетание тихого и уютного места и небольшого коворкинга, где можно приятно провести время, а также подготовится к занятиям, либо и вовсе заняться работой. У нас есть разнообразные настольные игры, Play Station, Free Wi-Fi и многое другое. В антикафе находится небольшой кинозал с 3D очками на небольшую компанию, в котором Вы можете по предварительной записи посмотреть свой фильм всего за 499р. Вашем распоряжении также есть огромное количество чая на выбор, разный кофе, печенье, вафли, пряники и т.п.<br />В антикафе есть два компьютера, за которыми можно поработать и подготовиться по учёбе. В Вашем распоряжении за небольшую плату ( 3р/лист чб, и 5р/лист цветная печать) будут принтеры для того, чтобы Вы смогли распечатать свою информацию на них.
				</div>
			</div>
		</div>
		<div class="parallax__layer parallax__layer--back">
		</div>
	</div>
	<div id="group5" class="parallax__group">
		<div class="parallax__layer parallax__layer--base" style="height: 100%;position: relative;">
			<div style="    width: 1140px; height: 600px;  margin: 0 auto 0;">

				<div class="c_4_1">
					г. Москва, ул. Сельскохозяйственная, д. 15 к. 2
				</div>
				<div class="c_4_2">телефон :  8 (499) 189-87-92 </div>
				<div class="c_4_4">МЫ ТУТ<br>
					<img src="wp-content/themes/twentythirteen/images/wearehere.png" alt>
				</div>
				
				<div class="c_4_7">Присоединяйтесь к нам в социальных сетях</div>

				<div class="c_4_5">
					<a target="_blank" class="hvr-buzz-out" href="https://www.facebook.com/groups/TimeHouse/"><img onmouseover="this.src='wp-content/themes/twentythirteen/images/sn_footer_fb_hover.png';" onmouseout="this.src='wp-content/themes/twentythirteen/images/sn_footer_fb.png';"  src="wp-content/themes/twentythirteen/images/sn_footer_fb.png" alt></a>
					<a target="_blank" class="hvr-buzz-out" href="http://instagram.com/anticafe_timehouse/"><img onmouseover="this.src='wp-content/themes/twentythirteen/images/sn_footer_insta_hover.png';" onmouseout="this.src='wp-content/themes/twentythirteen/images/sn_footer_insta.png';"  src="wp-content/themes/twentythirteen/images/sn_footer_insta.png" alt></a>
					<a target="_blank" class="hvr-buzz-out" href="http://vk.com/time_house"><img  onmouseover="this.src='wp-content/themes/twentythirteen/images/sn_footer_vk_hover.png';" onmouseout="this.src='wp-content/themes/twentythirteen/images/sn_footer_vk.png';" src="wp-content/themes/twentythirteen/images/sn_footer_vk.png" alt></a>
				</div>

				<div class="c_4_6">
					<div class="c_4_6_1">Наши партнеры </div>
					<div class="c_4_6_2"><a style="    color: orange!important;" href="http://baikalmoscow.ru/">Гостиница «Байкал»</a></div>
				</div>
			</div>
			<div class="c_4_3">
				<a href="http://goo.gl/maps/MJPnr" target="_blank" ><img src="wp-content/themes/twentythirteen/images/map.png" alt /></a>
			</div>
		</div>
	</div>

</div>
<!--<div class="c_1">
</div>
<div class="c_2">

</div>
<div class="c_3">

</div>
<div class="c_5">

</div>
<div class="c_4">

</div>


	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		</div>
	</div>-->
<script>
	$( window ).load(function() {

		$('#map_maap').contents().find('#mapDiv').css("border-radius","50%");
	});


</script>
	<script src="wp-content/themes/twentythirteen/js/jquery-1.11.0.min.js"></script>
	<script src="wp-content/themes/twentythirteen/js/lightbox.js"></script>

<?php get_footer(); ?>

